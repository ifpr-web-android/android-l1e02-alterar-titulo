package br.edu.ifpr.android.l1e2alomundotituloapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AloMundoTituloActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_mundo_titulo);
        TextView alo_mundo = (TextView) findViewById(R.id.alo_mundo);
        alo_mundo.setText(getString(R.string.alo_mundo));
    }
}
